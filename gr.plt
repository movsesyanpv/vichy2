set terminal png interlace font arial 14 size 1920,1200 nocrop
# set terminal postscript eps enhanced size 50,40
#set xrange[0:0.3]
set output "res.png"
plot "res.dat" u 1:2 t "y", "res.dat" u 1:3 t "z"
